<div id="phone-modal-bck"></div>
<div id="phone-modal">
    <div class="text">
        <div class="title">Napíšte nám</div>
        <p>a my Vás budeme kontaktovať</p>
    </div>

    <div>
        <input class="name" type="text" placeholder="Meno a prezvisko">
        <input class="phone" type="text" placeholder="Telefón">
    </div>

    <div>
        <button class="send">
            ODOSLAŤ
        </button>
        <button class="close-modal">
            ZRUŠIŤ
        </button>
    </div>
</div>
