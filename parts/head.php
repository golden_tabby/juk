<?php
$title = '';
if ($_SERVER['REQUEST_URI'] === '/') {
  $title = 'O NÁS';
}
if ($_SERVER['REQUEST_URI'] === '/sluzby') {
  $title = 'SLUŽBY';
}
if ($_SERVER['REQUEST_URI'] === '/cennik') {
  $title = 'CENNÍK';
}
if ($_SERVER['REQUEST_URI'] === '/kontakt') {
  $title = 'KONTAKT';
}
if (strpos($_SERVER['REQUEST_URI'], '/blog') === 0) {
  $title = 'BLOG';
}
?>

<title><?php echo $title ?> - ZDENT CLINIC</title>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
<link rel="manifest" href="/images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

<?php

require_once __DIR__ . '/../rootDir.php';
$appCss = ROOT_DIR . '/css/app.css';
$appJs = ROOT_DIR . '/js/app.js';

$lastModifiedTimestampCSS = filemtime($appCss);
$lastModifiedTimestampJS = filemtime($appJs);

?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="/css/app.css?<?php echo $lastModifiedTimestampCSS; ?>">

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/js/app.js?<?php echo $lastModifiedTimestampJS; ?>"></script>
<!--<script>-->
<!--  window.fbAsyncInit = function () {-->
<!--    FB.init({-->
<!--      appId: '3255795621108488',-->
<!--      cookie: true,-->
<!--      xfbml: true,-->
<!--      version: 'v7.0'-->
<!--    });-->
<!---->
<!--    FB.AppEvents.logPageView();-->
<!---->
<!--    FB.api(-->
<!--      '/103716771385958/feed',-->
<!--      function (response) {-->
<!--        if (response && !response.error) {-->
<!--          console.log(response);-->
<!--        }-->
<!--      }-->
<!--    );-->
<!--  };-->
<!--</script>-->
<!--<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>-->
