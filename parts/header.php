<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function () {
    FB.init({
      xfbml: true,
      version: 'v7.0'
    });
  };

  (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/sk_SK/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="103716771385958"
     theme_color="#7490AA"
     logged_in_greeting="Dobrý deň. Poradím Vám nejako?"
     logged_out_greeting="Dobrý deň. Poradím Vám nejako?">
</div>


<div id="header" class="clearfix">
    <div class="container">
        <div class="logo">
            <a href="/" title="Zdent Clinic">
                <img width="204" height="74" src="/images/logo.png" alt="Zdent Clinic">
            </a>
        </div>
        <div class="menu">
            <ul>
                <li id="menu-item-o-nas"<?php echo $_SERVER['REQUEST_URI'] === '/' ? " class='active'" : '' ?>>
                    <a href="/" title="O NÁS">
                        O NÁS
                    </a>
                </li>
                <li id="menu-item-sluzby"<?php echo $_SERVER['REQUEST_URI'] === '/sluzby' ? " class='active'" : '' ?>>
                    <a href="/sluzby" title="SLUŽBY">
                        SLUŽBY
                    </a>
                </li>
                <li id="menu-item-cennik"<?php echo $_SERVER['REQUEST_URI'] === '/cennik' ? " class='active'" : '' ?>>
                    <a href="/cennik" title="CENNÍK">
                        CENNÍK
                    </a>
                </li>
                <li id="menu-item-kontakt"<?php echo $_SERVER['REQUEST_URI'] === '/kontakt' ? " class='active'" : '' ?>>
                    <a href="/kontakt" title="KONTAKT">
                        KONTAKT
                    </a>
                </li>
            </ul>
        </div>
        <div class="phone">
            <a href="tel:+421919471768">
                +421 919 471 768
            </a>
        </div>
        <div class="button">
            <button>
                OBJEDNAŤ SA
            </button>
        </div>
    </div>

    <button class="hamburger hamburger--slider" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
    </button>
</div>
