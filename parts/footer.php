<?php require_once __DIR__ . '/../rootDir.php'; ?>
<?php include_once(ROOT_DIR . '/parts/phone-modal.php'); ?>
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-6">
                <div class="title">KONTAKT</div>
                <br>
                <div class="address">
                    Zdent clinic<br>
                    Františkánska 7538/6, 917 01 Trnava
                </div>
                <br>
                <div class="phone-email">
                    +421 919 471 768<br>
                    zdentclinic@gmail.com
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="title">ORDINAČNÉ HODINY</div>
                <div class="schedule">
                    PO: 08.00 – 16.00<br>
                    UT: 08.00 – 16.00<br>
                    ST: 08.00 – 16.00<br>
                    ŠT: 08.00 – 16.00<br>
                    PIA: 08.00 – 16.00<br>
                    SO, NE:Na objednávku
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="title">SLUŽBY</div>
                <div class="services">
                    <a href="/cennik#vyšetrenie">Dentálna hygiena</a><br>
                    <a href="/cennik#endodoncia">Zubná endodoncia</a><br>
                    <a href="/cennik#chirurgia">Stomatologická chirurgia</a><br>
                    <a href="/cennik#dentálna_hygiena_a_profylaxia">Estetická stomatológia</a><br>
                    <a href="/cennik#záchovná_stomatologia">Záchovná stomatológia</a><br>
                    <a href="/cennik#protetika">Zubná protetika</a><br>
                    <a href="/cennik#detska_stomatologia">Detská stomatológia</a>
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="title">OSTAŇTE S NAMI</div>
                <a href="/blog">Zdent Blog</a><br>
                <a href="#">Hodnotenia</a>
            </div>
        </div>
    </div>
</div>

<div class="scroll-up">&uarr;</div>
