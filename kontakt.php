<html lang="sk">
<head>
  <?php include_once('parts/head.php'); ?>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkvsYA2Jv1gY6btZ7cIzcnjCRa_o7zR0M&callback=initMap&libraries=&v=weekly" defer></script>
</head>
<body>
<?php include_once('parts/header.php'); ?>

<div class="kontakt-page">
    <div class="top">
        <div class="h1-wrapper">
            <h1>Kontakt</h1>
        </div>
    </div>

    <div class="full-width-wrapper clearfix">
        <div class="info">
            <div class="inner">
                <div class="title main">
                    Ordinačné hodiny
                </div>
                <p class="p1">Pondelok – Piatok 08:00 – 16:00</p>
                <p class="p2">Mimo ordinačných hodín sme Vám k dispozícii<br>po telefonickom dohovore aj cez víkend</p>

                <div class="title tel">Tel.: +421 919 471 768</div>
                <div class="title address">Františkánska 7538/6,<br>917 01 Trnava</div>
                <p class="p3">Zdent clinic s.r.o, zapísaný v obchodnom registr<br>okresného súdu Žilina, Vložka číslo: 74460/L</p>
                <p class="ico">IČO 53022017 DIČ 2121221850</p>

                <div class="socials">
                    <a class="fb" href="https://www.facebook.com/zdent.clinic"></a>
                    <a class="ig" href="#"></a>
                </div>
            </div>
        </div>
        <div id="map"></div>
    </div>
</div>

<?php include_once('parts/footer.php'); ?>
</body>
</html>
