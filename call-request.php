<?php

require_once('vendor/autoload.php');

use PHPMailer\PHPMailer\PHPMailer;

$phone = preg_replace('/[^0-9]/', '', $_POST['phone']);
$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);

try {
  $mail = new PHPMailer();

  $mail->IsSMTP();
  $mail->CharSet = 'UTF-8';

  $mail->Host = "127.0.0.1"; // SMTP server example
  $mail->SMTPDebug = false;                     // enables SMTP debug information (for testing)
  $mail->SMTPAuth = false;                  // enable SMTP authentication
  $mail->Port = 25;                    // set the SMTP port for the GMAIL server
//$mail->Username   = "username"; // SMTP account username example
//$mail->Password   = "password";        // SMTP account password example

  $mail->setFrom('no-reply@zdent.com', 'ZDENT');
  $mail->addAddress('joe@example.net', 'Joe User');

  $mail->isHTML(true);
  $mail->Subject = 'Zdent: Callback request!';
  $mail->Body = 'Name: ' . $name . '<br> Phone: ' . $phone;
  $mail->AltBody = '';

  $mail->send();

  echo '1';
  exit;
} catch (\Exception $e) {
  echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
