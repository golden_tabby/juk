/** Required gulp file */
var gulp = require('gulp');
var plumber = require('gulp-plumber');

/**Scss compiler
 * manually compile by using : gulp sass
 */
var sass = require('gulp-sass');


gulp.task('sass', function () {
    return gulp.src('scss/app.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(gulp.dest('css'))
});

gulp.task('watch', function () {
    gulp.watch('scss/*.scss', ['sass']);
    // gulp.watch('app/js/*.js', ['js']);
});

gulp.task('default', ['sass', 'watch']);
