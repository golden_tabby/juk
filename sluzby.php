<html lang="sk">
<head>
  <?php include_once('parts/head.php'); ?>
</head>
<body>
<?php include_once('parts/header.php'); ?>


<div class="sluzby-page">
    <div class="top">
        <div class="h1-wrapper">
            <h1>VSTUPNÁ PREHLIADKA</h1>
        </div>

        <p>
            Pozostáva z komplexnej kontroly chrupu spojenej s RTG diagnostikou
            a stanovením liečebného plánu podla potreby klienta.
        </p>
    </div>

    <div class="container sluzby">
        <div class="row">
            <div class="col-md-6 item pink">
                <div class="align-center">
                    <div class="icon-wrapper">
                        <div class="icon">
                            <img src="/images/slugbu/dentalna_hygiena.png" alt="">
                        </div>
                    </div>
                </div>

                <div class="title">
                    Dentálna hygiena
                </div>
                <ul>
                    <li>odstránenie zubného kameňa</li>
                    <li>odstránenie pigmentových škvŕn (air flow)</li>
                    <li>bielenie zubov</li>
                    <li>fluoridácia</li>
                </ul>
            </div>

            <div class="col-md-6 item blue">
                <div class="align-center">
                    <div class="icon-wrapper">
                        <div class="icon">
                            <img src="/images/slugbu/esteticka.png" alt="">
                        </div>
                    </div>
                </div>

                <div class="title">
                    Estetická stomatológia
                </div>
                <ul>
                    <li>zubná korunka alebo mostík</li>
                    <li>inlay, onlay, fazety</li>
                    <li>snímatelné náhrady</li>
                </ul>
            </div>

            <div class="col-md-6 item green">
                <div class="align-center">
                    <div class="icon-wrapper">
                        <div class="icon">
                            <img src="/images/slugbu/zachovna.png" alt="">
                        </div>
                    </div>
                </div>

                <div class="title">
                    Záchovná stomatológia
                </div>
                <ul>
                    <li>Endodoncia – ošetrenie koreňových kanálikov</li>
                    <li>biele fotokompozitné výplňe (plomby)</li>
                    <li>pečatenie zubov</li>
                </ul>
            </div>

            <div class="col-md-6 item yellow">
                <div class="align-center">
                    <div class="icon-wrapper">
                        <div class="icon">
                            <img src="/images/slugbu/chirurgia.png" alt="">
                        </div>
                    </div>
                </div>

                <div class="title">
                    Stomatochirurgia
                </div>
                <ul>
                    <li>chirurgické extrakcie</li>
                    <li>resekcie koreňového hrotu</li>
                    <li>implantáty</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php include_once('parts/footer.php'); ?>
</body>
</html>
