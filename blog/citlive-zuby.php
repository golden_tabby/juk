<html lang="sk">
<head>
  <?php include_once('../parts/head.php'); ?>
</head>
<body>
<?php include_once('../parts/header.php'); ?>

<div class="blog-item-page">
    <h1 class="top">
        <div class="h1-wrapper">
            <h1>Citlivé zuby - aký je dôvod?</h1>
        </div>
    </h1>
    <div class="container">
        <img class="main-img" width="40%" src="/images/blog/blog-1-main.png" alt="">
        <div class="text">
            <p>
                Vždy jete zmrzlinu s obavou a trpezlivo čakáte, kým sa káva ochladí? Ak cítite prenikavú bolesť zubov a podráždenie pri pití teplých nápojov, máte zvýšenú citlivosť zubov alebo odborne povedané <strong>hyperestéziu</strong>. Podľa štatistík je to nepríjemný jav pozorovaný u viac ako 40% populácie.
            </p>
            <h2>Zub a jeho stavba</h2>
            <p>
                Najprv treba zistiť odkiaľ bolesť pochádza. Ak to chcete urobiť, musíte pochopiť, ako je zub zložený. Pod najtvrdším tkanivom v tele - zubnou sklovinou - sa nachádza mäkší dentín, do ktorého vystupujú mikrotubuly s nervovými zakončeniami z dolnej mäkkej vrstvy zubu (pulpa). Bolestivosť a citlivosť sa objavia, keď sa dentálne tubuly otvoria. Najčastejšie sa to stáva, keď sa zrúti sklovina a dentín zostane nechránený.
            </p>
            <h2 style="clear:left;">Prečo je to tak?</h2>
            <p>
                Ak hovoríme o dentálnych príčinách, potom je citlivosť zubov najčastejšie spôsobená týmito problémami:
            </p>
            <ul>
                <li>kaz a jeho komplikácie</li>
                <li>zápal zubného nervu</li>
                <li>úraz zubov</li>
                <li>obnosená plomba</li>
                <li>ochorenie ďasien</li>
                <li>rednutie zubnej skloviny</li>
                <li>obnaženie koreňa zuba</li>
                <li>patologické odieranie zubov</li>
                <li>nesprávna technika umývania zubov</li>
                <li>zubná kefka so zvýšenou tuhosťou</li>
                <li>nadmerná konzumácia kyslých potravín, sladkostí a sýtených nápojov</li>
                <li>chybná technika bielenia zubov</li>
            </ul>
            <p>
                Zuby môžu niekedy zostať citlivé po stomatologických zákrokoch ako je napríklad  odstránenie zubného kameňa alebo ošetrenie zubného kazu. Tento nepríjemný pocit je krátkodobý a rýchlo prestane. Citlivosť vyliečeného zuba by sa mala vrátiť do normálu do 1 – 2 týždňov. V opačnom prípade musíte kontaktovať svojho zubára.
            </p>
            <h2>Ako bojovať proti citlivosti zubov?</h2>
            <p>
                Ak máte nepríjemné pocity, mali by ste sa skontaktovať so svojím zubným lekárom a absolvovať špeciálnu liečbu. Lekár vykoná diagnostiku, aby zistil príčinu citlivosti zubov a vykoná potrebné úkony, aby uzavrel nervové kanáliky, posilnil sklovinu a odstránil bolesť.
            </p>
            <h2>Fluoridácia – zmiernenie citlivosti zubov</h2>
            <p>
                Ak nie je potrebné zubné ošetrenie, potom sa na odstránenie citlivosti zubov aplikuje špeciálny lak, ktorý obsahuje fluór a ďalšie minerálne látky (vápnik, draslík, horčík). Jeho zloženie vyrovnáva nedostatok mikroelementov v sklovine a uzatvára mikropóry a tubuly dentínu. Sklovina je posilnená, nielen chráni dentín pred agresívnymi faktormi, ale môže tiež úspešne odolávať zubnému kazu. Táto metóda sa dokonca používa na liečbu zubného kazu v jeho počiatočných štádiách.
            </p>
            <h2>Časté faktory ovplyvňujúce citlivosť zubov</h2>
            <p>
                V niektorých prípadoch je citlivosť výsledkom celkového stavu tela. Niekedy môže prejsť časom aj bez zubného ošetrenia. Nesprávna strava a nedostatok základných minerálov ovplyvňujú stav vašich zubov. Niektoré choroby spôsobujú metabolické poruchy, ktoré tiež vplývajú na zubnú sklovinu. V 80% prípadov hyperestézie sa v endokrinnom systéme vyskytujú abnormality. Preto venujte pozornosť citlivosti zubov ako signálu vášho tela o potrebe konzultácie s endokrinológom.
            </p>
            <h2>Na čo sa zamerať, ak máte citlivé zuby</h2>
            <p>
                * Vyvážená strava<br>
                Sledujete, či vaše telo prijíma všetky užitočné látky? Osobitná pozornosť by sa mala venovať vitamínu A (zdroje – vajcia, pečeň, mrkva), pretože jeho nedostatok spôsobuje zvýšenú citlivosť zubov.
            </p>
            <p>
                * Škodlivé výrobky<br>
                Ak máte radi sladké, kyslé a sýtené nápoje, budete nútení trápiť sa viac – požívanie takýchto výrobkov by sa malo obmedziť, ale je lepšie úplne ich vylúčiť zo stravy. Kyseliny spôsobujú zvýšenú citlivosť zubov.
            </p>
            <p>
                * Správna hygiena<br>
                Ak vás trápi citlivosť zubov, zmeňte zubnú kefku na mäkkú a začnite používať zubné pasty na citlivé zuby. Pri čistení zubov postupujte tak, aby sa štetiny pohybovali vertikálne, od ďasien po reznú hranu.
            </p>
            <p>
                Pri pretrvávajúcich problémoch príďte do Zdent clinic. Naši odborníci určia príčiny citlivosti zubov, vykonajú účinnú liečbu a odstránia nepríjemné pocity na dlhú dobu. Dostanete tiež účinné odporúčania týkajúce sa starostlivosti o vaše citlivé zuby.
            </p>
            <h2>Sťažené prerezávanie zubov múdrosti</h2>
            <p>
                Zuby múdrosti rastú osobitne. S vývojom človeka a prechodom na mäkkú stravu sa osmičky stali nepotrebnými. Už neplnia funkciu ako predtým. Veľkosť čeľuste sa výrazne zmenšila. Preto sa zuby múdrosti <strong>prerezávajú</strong> s toľkými ťažkosťami. Tento proces niekedy trvá 2 až 5 rokov a takmer vždy spôsobuje bolesť a nepríjemnosti.
            </p>
            <h2>Možné komplikácie pri raste zubov múdrosti</h2>
            <p>
                * Nedostatok miesta<br>
                V tomto prípade sa osmička veľmi ťažko prerezáva správne bez toho, aby sa neposunuli ďalšie zuby v rade. Je tu veľa zubov a s tým sú spojené aj hygienické problémy.
            </p>
            <p>
                * V nesprávnom uhle<br>
                Ak zub múdrosti rastie v nesprávnom uhle alebo takpovediac „leží“, pritlačí na susedné zuby a často ich poškodí. Aby sa to nestalo stoličku, zub múdrosti, treba vytrhnúť.
            </p>
            <p>
                * Zápal<br>
                Ôsmy zub sa môže <strong>prerezať</strong> správne, ale v dôsledku zlej hygieny dochádza k zápalu. Ak sa proces nezastaví včas, bude potrebné zub odstrániť.
            </p>
            <p>
                * Zubný kaz<br>
                Dlhé prerezávanie a zlá hygiena môžu spôsobiť zubný kaz na poslednej stoličke. V dôsledku ťažkého prístupu k takémuto zubu je často nemožné vykonať vysokokvalitné ošetrenie a ôsmy zub musí byť odstránený.
            </p>
            <p>
                Ak pri prerezávaní zubu múdrosti cítite vážne nepohodlie, musíte sa poradiť s lekárom. Zubár vám spraví RTG snímok a zvolí ďalší vhodný lekársky postup. Môže urobiť <strong>chirurgické uvoľnenie mäkkých tkanív pod anestézou</strong> alebo odstrániť zub.
            </p>
            <p>
                V prípade, ak trpíte podobnými problémami, neváhajte sa na nás obrátiť. Náš profesionálny tím Zdent clinic vám rád poradí a pomôže.
            </p>
            <p>
                Spojte sa s nami telefónom na …….. alebo mailom na ………...
            </p>
        </div>
    </div>
</div>

<?php include_once('../parts/footer.php'); ?>
</body>
</html>
