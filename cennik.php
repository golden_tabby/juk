<html lang="sk">
<head>
  <?php include_once('parts/head.php'); ?>
</head>
<body>
<?php include_once('parts/header.php'); ?>

<div class="cennik-page">
    <div class="h1-wrapper">
        <h1>CENNÍK</h1>
    </div>

    <div class="container">
        <div class="services-icons clearfix">
            <div class="item active">
                <a href="#vyšetrenie">
                    <div class="align-center">
                        <div class="icon-wrapper">
                            <div class="icon" style="background-image: url(/images/cennik/1.png)">
<!--                                <img width="151" src="/images/cennik/1.png" alt="Vyšetrenie">-->
                            </div>
                        </div>
                    </div>
                    <span class="title">
                        Vyšetrenie
                    </span>
                </a>
            </div>
            <div class="item">
                <a href="#záchovná_stomatologia">
                    <div class="align-center">
                        <div class="icon-wrapper">
                            <div class="icon" style="background-image: url(/images/cennik/2.png)">
<!--                                <img width="151" src="/images/cennik/2.png" alt="Záchovná stomatologia">-->
                            </div>
                        </div>
                    </div>
                    <span class="title">
                    Záchovná stomatologia
                    </span>
                </a>
            </div>
            <div class="item">
                <a href="#endodoncia">
                    <div class="align-center">
                        <div class="icon-wrapper">
                            <div class="icon" style="background-image: url(/images/cennik/3.png)">
<!--                                <img width="151" src="/images/cennik/3.png" alt="Endodoncia">-->
                            </div>
                        </div>
                    </div>
                    <span class="title">
                    Endodoncia
                    </span>
                </a>
            </div>
            <div class="item">
                <a href="#detska_stomatologia">
                    <div class="align-center">
                        <div class="icon-wrapper">
                            <div class="icon" style="background-image: url(/images/cennik/4.png)">
<!--                                <img width="151" src="/images/cennik/4.png" alt="Detska stomatologia">-->
                            </div>
                        </div>
                    </div>
                    <span class="title">
                    Detska stomatologia
                    </span>
                </a>
            </div>
            <div class="item">
                <a href="#protetika">
                    <div class="align-center">
                        <div class="icon-wrapper">
                            <div class="icon" style="background-image: url(/images/cennik/5.png)">
<!--                                <img width="151" src="/images/cennik/5.png" alt="Protetika">-->
                            </div>
                        </div>
                    </div>
                    <span class="title">
                    Protetika
                    </span>
                </a>
            </div>
            <div class="item">
                <a href="#chirurgia">
                    <div class="align-center">
                        <div class="icon-wrapper">
                            <div class="icon" style="background-image: url(/images/cennik/6.png);background-size: 45% 63%;">
<!--                                <img src="/images/cennik/6.png" alt="Chirurgia">-->
                            </div>
                        </div>
                    </div>
                    <span class="title">
                    Chirurgia
                    </span>
                </a>
            </div>
            <div class="item">
                <a href="#dentálna_hygiena_a_profylaxia">
                    <div class="align-center">
                        <div class="icon-wrapper">
                            <div class="icon" style="background-image: url(/images/cennik/7.png);background-size: 68% 63%;">
<!--                                <img src="/images/cennik/7.png" alt="">-->
                            </div>
                        </div>
                    </div>
                    <span class="title">
                        Dentálna hygiena a profylaxia
                    </span>
                </a>
            </div>
        </div>
    </div>


    <div class="tables">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pr-20">
                    <div class="table" id="vyšetrenie">
                        <div class="inner" style="background: rgba(242, 162, 177, 0.1);">
                            <div class="title">
                                Vyšetrenie, administrativa, RTG, anestetika
                            </div>
                            <div class="values">
                                <div class="value clearfix">
                                    <div class="name">
                                        Vstupné komplexné vyšetrenie + zalozenie karty
                                    </div>
                                    <div class="price">
                                        0
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Preventivna prehliadka
                                    </div>
                                    <div class="price">
                                        0
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Konzultácia
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Akútne ošetrenie, cudzieho pacienta
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Ošetrenie mimo ordinačných hodín
                                    </div>
                                    <div class="price">
                                        25
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Nedostavenie sa na termin oštetrenia bez zrušenia terminu 48h vopred
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Rtg Intraoralt
                                    </div>
                                    <div class="price">
                                        8
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Injekcia anestézia
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Injekcia anestézia 2x a viac
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Kofferdam, optrageat
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Test vitality zubov
                                    </div>
                                    <div class="price">
                                        5
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Vyšetrenie na fókusy
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table" id="záchovná_stomatologia">
                        <div class="inner" style="background: rgba(116, 144, 170, 0.1);">
                            <div class="title">
                                Záchovná stomatológia
                            </div>
                            <div class="values">
                                <div class="value clearfix">
                                    <div class="name">
                                        Fotokompozitná výplň — jedna plôška front
                                    </div>
                                    <div class="price">
                                        25
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Fotokompozitná výplň — dve plôška front
                                    </div>
                                    <div class="price">
                                        30
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Fotokompozitná výplň — tri plôška front
                                    </div>
                                    <div class="price">
                                        35
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Rekonštrukcia korunký zuba front
                                    </div>
                                    <div class="price">
                                        60
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Fotokompozitná výplń — jedna plôška dist.
                                    </div>
                                    <div class="price">
                                        45
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Fotokompozitná výplń — dve plôška dist.
                                    </div>
                                    <div class="price">
                                        50
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Fotokompozitná výplń —tri plôška dist.
                                    </div>
                                    <div class="price">
                                        55
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Rekonštrukcia korunký zuba dist.
                                    </div>
                                    <div class="price">
                                        65
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Dlahovanie zubov (1 zub)
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Výplň FUJI (SIC)
                                    </div>
                                    <div class="price">
                                        25
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Dalsa kapsula FUJI
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Zavedenie čapu zo sklenených vlákien a fixácia
                                    </div>
                                    <div class="price">
                                        30
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Podložka (MTA, Сalcimol,LIFE)
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Podložka — ionoseal, theracal, Calcimol
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Dočasna plomba
                                    </div>
                                    <div class="price">
                                        12
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table" id="chirurgia">
                        <div class="inner" style="background: #f5eded;">
                            <div class="title">
                                Chirurgia
                            </div>
                            <div class="values">
                                <div class="value clearfix">
                                    <div class="name">
                                        Extrakcia trvalého 1 — koreňového zuba
                                    </div>
                                    <div class="price">
                                        28
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Extrakcia trvalého viackoreňového zuba
                                    </div>
                                    <div class="price">
                                        35
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Ošetrenie sťaženého prerezávania
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Sutura extračnej rany
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Chirurgická revizía rany (alveolitída)
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Komplikovaná extrakcia
                                    </div>
                                    <div class="price">
                                        50
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Ošetrenie a kontrola po extrakcii
                                    </div>
                                    <div class="price">
                                        5
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Chirurgická extrakcia
                                    </div>
                                    <div class="price">
                                        150
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Gingivotomia(uprava dásna)
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pl-20">
                    <div class="table" id="endodoncia">
                        <div class="inner" style="background: rgba(186, 217, 191, 0.1);">
                            <div class="title">
                                Endodoncia — ošetrenie koreńiového kanálika
                            </div>
                            <div class="values">
                                <div class="value clearfix">
                                    <div class="name">
                                        Použitie MTA
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Vnútorné bielenie zuba
                                    </div>
                                    <div class="price">
                                        25
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Devitalizujúca vložka
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Predendo dostavba
                                    </div>
                                    <div class="price">
                                        20-40
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Endodontické ošetrenie 1 koreňiovy kanálik
                                    </div>
                                    <div class="price">
                                        30
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Endodontické ošetrenie 2 - 4 kanálika
                                    </div>
                                    <div class="price">
                                        50
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Odstránenie jednej starej koreňovej výplňe
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Odstránenie skleného čapu z kanáliku
                                    </div>
                                    <div class="price">
                                        25
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Trepanácia koreňových kanálikov
                                    </div>
                                    <div class="price">
                                        18
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Dočasná koreňová výplň (ultracal)
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Odstránenie zalomeného koreňového nástroja
                                    </div>
                                    <div class="price">
                                        50-100
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Endodontický výplachový protokol
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table" id="protetika">
                        <div class="inner" style="background: rgba(242, 212, 174, 0.1);">
                            <div class="title">
                                Protetika
                            </div>
                            <div class="values">
                                <div class="value clearfix">
                                    <div class="name">
                                        Celková snímateľná náhrada živicová
                                    </div>
                                    <div class="price">
                                        180
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Čiastočná snímateľná náhrada živicová
                                    </div>
                                    <div class="price">
                                        160
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Čiastočná snímateľná náhrada FLEXI
                                    </div>
                                    <div class="price">
                                        250
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Čiastočný snímateľný skelet
                                    </div>
                                    <div class="price">
                                        600
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Kovokeramická korunka
                                    </div>
                                    <div class="price">
                                        180
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Korunka celokeramická ZIRCON
                                    </div>
                                    <div class="price">
                                        260
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Korunka celokeramická EMAX
                                    </div>
                                    <div class="price">
                                        300
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Odtlačky A-silikón
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Odtlaćok — alginát
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Odtlačky С-silikón
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Dočasná korunka  1 kus
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Sťahovanie koruniek (1zub)
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Fazeta
                                    </div>
                                    <div class="price">
                                        320
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Opätovné nacementovanie korunky (1 zub)
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Nacementovanie korunky na dočasný cement
                                    </div>
                                    <div class="price">
                                        7
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Wax-up 1 zub
                                    </div>
                                    <div class="price">
                                        10
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Brusenie(1 zub)
                                    </div>
                                    <div class="price">
                                        5
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table" id="dentálna_hygiena_a_profylaxia">
                        <div class="inner" style="background: rgba(116, 144, 170, 0.1);">
                            <div class="title">
                                Dentálna hygiena a profylaxia
                            </div>
                            <div class="values">
                                <div class="value clearfix">
                                    <div class="name">
                                        Dentálna hygiena komplet
                                    </div>
                                    <div class="price">
                                        40
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Airflow
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Čistenie zubného kameňa ultrazvukom
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Bielenie zubov
                                    </div>
                                    <div class="price">
                                        100-400
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Fluoridácia zubov
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table" id="detska_stomatologia">
                        <div class="inner" style="background: #f8fbf8;">
                            <div class="title">
                                Detska stomatologia
                            </div>
                            <div class="values">
                                <div class="value clearfix">
                                    <div class="name">
                                        Výplň milečneho zuba
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Koreňova výpln milečneho zuba
                                    </div>
                                    <div class="price">
                                        25
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Extrakcia milečneho zuba
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Pečatenie fisur(1zub)
                                    </div>
                                    <div class="price">
                                        15
                                    </div>
                                </div>
                                <div class="value clearfix">
                                    <div class="name">
                                        Detská dentálna hygiena, fluoridácia, nácvik ústnej hygieny
                                    </div>
                                    <div class="price">
                                        20
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once('parts/footer.php'); ?>
</body>
</html>
