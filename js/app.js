var map;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: {lat: 48.379093, lng: 17.582912},
    zoom: 17
  });

  var marker = new google.maps.Marker({
    // The below line is equivalent to writing:
    // position: new google.maps.LatLng(-34.397, 150.644)
    position: {lat: 48.379093, lng: 17.582912},
    map: map
  });
}

$(function () {
  $('#header .button button').click(function () {
    $('#phone-modal-bck').show();
    $('#phone-modal').show();
  });

  $('#phone-modal-bck, #phone-modal .close-modal').click(function () {
    $('#phone-modal-bck').hide();
    $('#phone-modal').hide();
  });

  $('#phone-modal .send').click(function () {
    var nameVal = $('#phone-modal .name').val();
    var phoneVal = $('#phone-modal .phone').val().replace(/\D+/g, '');

    if (phoneVal.length !== 12) {
      $('#phone-modal .phone').addClass('error');

      return;
    }

    $.post('/call-request', {name: nameVal, phone: phoneVal}, function (response) {
      if (response == 1) {
        $('#phone-modal .name').val('');
        $('#phone-modal .phone').val('');
      }
    });
  });

  $('#phone-modal .phone').keyup(function () {
    $(this).removeClass('error');
  });

  $('.hamburger').click(function (e) {
    e.stopPropagation();

    if ($(this).hasClass('is-active')) {
      $(this).removeClass('is-active');

      $('.menu').removeClass('mobile-active');
    } else {
      $(this).addClass('is-active');

      $('.menu').addClass('mobile-active');
    }
  });


  $(window).scroll(function(e) {
    if ($(document).scrollTop() === 0) {
      $('.scroll-up').hide();
    } else {
      if (!$('.scroll-up').is(':visible')) {
        $('.scroll-up').show();
      }
    }
  });

  $('.scroll-up').click(function() {
    $('html,body').animate({scrollTop: 0},'slow');
  });
});
