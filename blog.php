<html lang="sk">
<head>
  <?php include_once('parts/head.php'); ?>
</head>
<body>
<?php include_once('parts/header.php'); ?>

<div class="blog-page">
    <div class="top">
        <div class="h1-wrapper">
            <h1>Zdent Blog</h1>
        </div>
    </div>

    <div class="posts">
        <a class="post" href="/blog/citlive-zuby">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-5">
                        <div class="image-wrapper">
                            <img src="/images/blog/blog1.png" alt="Citlivé zuby">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="text">
                            <h2>Citlivé zuby - aký je dôvod?</h2>
                            <p>Vždy jete zmrzlinu s obavou a trpezlivo čakáte, kým sa káva ochladí? Ak cítite prenikavú bolesť zubov a podráždenie pri pití teplých nápojov, máte zvýšenú citlivosť zubov alebo odborne povedané hyperestéziu. Podľa štatistík je to nepríjemný jav pozorovaný u viac ako 40% populácie...</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
<!--        <a class="post" href="/blog/">-->
<!--            <div class="container">-->
<!--                <div class="row clearfix">-->
<!--                    <div class="col-md-5">-->
<!--                        <div class="image-wrapper">-->
<!--                            <img src="/images/blog/blog2.png" alt="Citlivé zuby">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-7">-->
<!--                        <div class="text">-->
<!--                            <h2>Citlivé zuby</h2>-->
<!--                            <p>Всегда с опаской едите мороженное и терпеливо ждете, пока остынет кофе? Если вы испытываете пронзительную зубную боль от температурных или вкусовых раздражителей, то у вас повышенная чувствительность зубов или, по‑научному, гиперестезия. По статистике, это неприятное явление наблюдается у более чем 40% населения.</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </a>-->
    </div>
</div>

<?php include_once('parts/footer.php'); ?>
</body>
</html>
