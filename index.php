<html lang="sk">
<head>
  <?php include_once('parts/head.php'); ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkvsYA2Jv1gY6btZ7cIzcnjCRa_o7zR0M&callback=initMap&libraries=&v=weekly" defer></script>
</head>
<body>
<?php include_once('parts/header.php'); ?>

<div class="home-page">
    <div class="banner">
        <img src="/images/home-banner.jpg" alt="">
    </div>

    <div class="content">
        <div class="h1-wrapper">
            <h1>
                O NÁS
            </h1>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-6 photo">
                    <img src="/images/taras-photo.jpg" alt="">
                </div>
                <div class="col-md-6 col-sm-6 col-6 text">
                    <p>
                        ​​Filozofia Zdent Clinic je založená na čestných a otvorených vzťahoch s pacientom.V našej klinike použivame najmodernejšie technológií
                        a neustále zdokonaľuje svoje lekárske vedomosti.
                    </p>
                    <p>
                        Najdôležitejšie su kvalita a bezbolestnosť liečby pacientov na našej klinike.V našej praxe uplatňujeme najmodernejšie a najúčinnejšie
                        technologie ktore zaručia vašu spokojnosť.
                    </p>
                    <p>
                        Cieľom našej práce je vždy poskytovať najkompletnejší stomatologicke službý.
                    </p>
                    <p>
                        Ceníme si že ste si nas vybralý a chceme aby ste vždy vedeli že Vam aj blizkim pomožeme vyrešit akykolvek problem.
                    </p>
                    <p>
                        Naša priorita je spokojnost pacienta.
                    </p>
                    <p>
                        Začnite sa z nami usmievať.
                    </p>

                    <p class="mobile-show">
                        ​​Filozofia Zdent Clinic je založená na čestných a otvorených vzťahoch s pacientom.V našej klinike použivame najmodernejšie technológií
                        a neustále zdokonaľuje svoje lekárske vedomosti.
                    </p>
                    <p class="mobile-show">
                        Naša priorita je spokojnost pacienta.
                    </p>
                    <p class="mobile-show">
                        Začnite sa z nami usmievať.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="kontakt">
        <div class="h1">
            <span>
                Kontakt
            </span>
        </div>
        <div class="title main">
            Ordinačné hodiny
        </div>
        <p class="p1">Pondelok – Piatok 08:00 – 16:00</p>
        <p class="p2">Mimo ordinačných hodín sme Vám k dispozícii<br>po telefonickom dohovore aj cez víkend</p>

        <div class="title tel">Tel.: +421 919 471 768</div>
        <div class="title address">Františkánska 7538/6,<br>917 01 Trnava</div>
        <p class="p3">Zdent clinic s.r.o, zapísaný v obchodnom registr<br>okresného súdu Žilina, Vložka číslo: 74460/L</p>
        <p class="ico">IČO 53022017 DIČ 2121221850</p>

        <div class="socials">
            <a class="fb" href="https://www.facebook.com/zdent.clinic"></a>
            <a class="ig" href="#"></a>
        </div>
    </div>

    <div id="map"></div>
</div>

<?php
//
//$page_name = 'zdent.clinic'; // Example: http://facebook.com/{PAGE_NAME}
//$page_id = '103716771385958'; // can get form Facebook page settings
//$app_id = '3255795621108488'; // can get form Developer Facebook Page
//$app_secret = '17f0b33cbe0ee9d8b9ee4bcdafb2f4c9'; // can get form Developer Facebook Page
//$limit = 5;
//
//function load_face_posts($page_id, $page_name, $app_id, $app_secret) {
//  $access_token = "https://graph.facebook.com/oauth/access_token?client_id=$app_id&client_secret=$app_secret&grant_type=client_credentials";
//  $access_token = file_get_contents($access_token); // returns 'accesstoken=APP_TOKEN|APP_SECRET'
//  $access_token = json_decode($access_token, true);
//  //var_dump($access_token);exit;
//  $access_token['access_token'] = 'EAAuRIP11GwgBANfc3m3eKBbD1EFY7axAwPqBxZBYw5ZBtE9FtMiWr2wvAD7EFYLVwkXuYVrp5HHBaZAvcoMWxr3kDmqwR5XZBvxbb3WqbXKKJ0j1r1Fx0CL6XrXPKFjcx9B7mqSnRBg05jjR8AaeqTFh9awkkrAAFZCRoFZAYtnf73jxG3SPWS5gmZCSRBtQZAkXiJxGxjGyjRvNhKSu2sdB';
//  $limit = 3;
//
//  $data  = file_get_contents("https://graph.facebook.com/$page_id/posts?limit=$limit&access_token={$access_token['access_token']}&fields=full_picture,picture,story,message");
//  $data = json_decode($data, true);
//  $posts = $data['data'];
//
//  var_dump($posts);exit;
//
//  for($i=0; $i<sizeof($posts); $i++) {
//    //echo $posts[$i][id];
//    $link_id = str_replace($page_id."_", '', $posts[$i][id]);
//    $message = $posts[$i][message];
//
//    echo ($i+1).". <a target='_blank' href='https://www.facebook.com/AqualinkMMC/posts/".$link_id."'>".$message."</a><br>";
//  }
//}
//
//load_face_posts($page_id, $page_name, $app_id, $app_secret);
//
//?>

<?php include_once('parts/footer.php'); ?>
</body>
</html>
